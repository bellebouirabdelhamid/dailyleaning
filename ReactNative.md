React Native
============

#### Creating new project : **npx react-native init (project name)**

#### Runing the application : 

- start the metro : **npx react-native start**

- start the application : **npx react-native run-android**

#### React native fundamentals:

- To define a component , we use JavaScript’s import to import (React) and (React Native)’s Core Components.

- React Native use **JSX (JavaScript XML)** .

- By nesting React Native's core components, we can create custom and reusable components.

- **Props** customize React components.

#### Core components and APIs :

###### Basic components :

- View : is a container that supports layout with "flexbox", "style", "some touch handling", and "accessibility controls". It is designed to be nested inside other views and can have 0 to many children of any type.

- Text : a component for displaying text. It supports "nesting", "styling" and touch handling.

- Image : a component for displaying different type of images . it support "styling" .

- TextInput : a component for inputting text into the app via a keyboard. It has a Props such as "auto-correction", "auto-capitalization", "placeholder", "onChangeText"...

- ScrollView : a scrollable container, which scrolls multiple child components and views inside it. The user can scroll the components in both direction **vertically** and **horizontally** (using the props **horizontal: true**).

- StyleSheet : is an abstraction similar to CSS StyleSheets. By grouping styles in one place , it is easier to understand the code.

------

###### User interface :

- Button : a component that works by clicking on it. It imports the "Button" class of "react-native".

- Switch : a Boolean control component which sets its value to true or false. It has an "onValueChange" callback method that updates its value prop.

-----

###### Android Components and APIs :

- BackHandler : The Backhandler API detects hardware button presses for "back navigation", lets you register event listeners for the system's back action, and lets you control how your application responds. It is Android-only.
**Pattern** : 
---------------------------------------------------------------------------
	BackHandler.addEventListener('hardwareBackPress', function () {
	  /**
	   * this.onMainScreen and this.goBack are just examples,
	   * you need to use your own implementation here.
	   *
	   * Typically you would use the navigator here to go to the last state.
	   */
	  if (!this.onMainScreen()) {
	    this.goBack();
	    /**
	     * When true is returned the event will not be bubbled up
	     * & no other back action will execute
	     */
	    return true;
	  }
	  /**
	   * Returning false will let the event to bubble up & let other event listeners
	   * or the system's default back action to be executed.
	   */
	  return false;
	});
-------------------------------------------------------------------------------

Methods "BackHandler. +":  **addEventListner(eventName,handler)** , **extiApp()** , **removeEventListner(eventName,handler)**

- DrawerLayoutAndroid : React component that wraps the platform "DrawerLayout" (Android only). The Drawer (typically used for navigation) is rendered with "renderNavigationView" and direct children are the main view (where your content goes). The "navigation view" is initially not visible on the screen, but can be pulled in from the side of the window specified by the "drawerPosition" prop and its width can be set by the "drawerWidth" prop.

Methods : **drawerPosition** , **drawerWidth** , **drawerBackgroundColor** , **renderNavigationView** ...

- PermissionsAndroid : "PermissionsAndroid" provides access to Android M's new permissions model. The so-called "normal" permissions are granted by default when the application is installed as long as they appear in "AndroidManifest.xml". (However, "dangerous" permissions require a dialog prompt. You should use this module for those permissions)

- ToastAndroid : Toasts are the technique in mobile development to notify the users about interrupting what they're doing.

-> React Native's Toast Android API exposes the Android platform's ToastAndroid module in the JS module.
It provides the thing (message, duration) method, which takes the below parameters:
Message: It is a string with the text to toast.
Duration: The duration of Toast is either the ToastAndroid.SHORT or ToastAndroid.LONG.

-> Alternatively, we can use the display with severity (message, duration, severity) to specify where the TToastappears on the screen layout. Maybe the ToastAndroid.TOP, ToastAndroid.BOTTOM, or ToastAndroid.CENTER.

###### Other Core components :

- ActivityIndicator : Displays a circular loading indicator.

- Alert : Launches an alert dialog with the specified title and message.

Optionally provide a list of buttons. Tapping any button will fire the respective onPress callback and dismiss the alert. By default, the only button will be an 'OK' button.

**Syntax** : "Alert.alert(title, message?, buttons?, options?)" ('?' optional)

- Animated : The "Animated" library is designed to make animations **fluid, powerful, and painless to build and maintain** . "Animated" focuses on declarative relationships between inputs and outputs, configurable transforms in between, and start/stop methods to control time-based animation execution.

- Dimensions : ***useWindowDimensions*** automatically updates ***width*** and ***height*** values when screen size changes. You can get your application window's width and height like so: "const { height, width } = **useWindowDimensions()**;"
properties : fontScale , height , scale , width .

- keyboardAvoidingView : It is a component to solve the common problem of views that need to move out of the way of the virtual keyboard. It can automatically adjust either its height, position, or bottom padding based on the keyboard height.

- Modal : provides a simple way to present content above an enclosing view . 

- PixelRatio : **PixelRatio** gives you access to the device's pixel density and font scale.

- RefreshControl : This component is used inside a scrollView to add pull to refresh functionality.When the ScrollView is at ***scrollY: 0***, swiping down triggers an ***onRefresh*** event.

- StatusBar : Component to control the app status bar . The status bar is the zone, typically at the top of the screen, that displays the current time, Wi-Fi and cellular network information, battery level and/or other status icons.