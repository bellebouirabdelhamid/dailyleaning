# Formik :

- Formik is a small library that helps you with the 3 most annoying parts:

1- Getting values in and out of form state.

2- Validation and error messages.

3- Handling form submission.

#### Install 'formik' :

- with npm : npm install formik 

- with yarn : yarn add formik .

## Validate with yup :

- yup is a package that works with Formik and validate the fields when submitting.

#### Install 'yup' :

- with npm : npm install yup .

- with yarn : yarn add yup .

## Exemple :

---------------------------------------------------------------------

    // Formik x React Native example
    import React from 'react';
    import {Button, Text, TextInput, View} from 'react-native';
    import {Formik, ErrorMessage} from 'formik';
    import * as yup from 'yup';

    const formScheema = yup.object({
    FirstName: yup.string().min(5).required(),
    LastName: yup.string().min(5).required(),
    });

    export default function MyReactNativeForm() {
    return (
        <Formik
        initialValues={{FirstName: '', LastName: ''}}
        onSubmit={values => console.log(values)}
        validationSchema={formScheema}>
        {({handleChange, handleBlur, handleSubmit, values, errors, touched}) => (
            <View>
            <TextInput
                style={{margin: 10, borderColor: 'black', borderWidth: 1}}
                onChangeText={handleChange('FirstName')}
                onBlur={handleBlur('FirstName')}
                value={values.FirstName}
            />
            {/*First way to show errors */}
            <Text>
                <ErrorMessage name="FirstName" />
            </Text>
            {/*--------------------------------------------- */}
            {/*Second way to show errors */}
            {/* <Text>{touched.FirstName && errors.FirstName}</Text> */}
            
            <TextInput
                style={{margin: 10, borderColor: 'black', borderWidth: 1}}
                onChangeText={handleChange('LastName')}
                onBlur={handleBlur('LastName')}
                value={values.LastName}
            />
            <Text>
                <ErrorMessage name="LastName" />
            </Text>
            {/* <Text>{touched.LastName && errors.FirstName}</Text> */}
            <View style={{width: 100, alignSelf: 'center', margin: 10}}>
                <Button onPress={handleSubmit} title="Submit" />
            </View>
            </View>
        )}
        </Formik>
    );
    }


---------------------------------------------------------------------------
