# chapter Git:

-> Git is Distributed Version Control System .It's developed to manage projects with high speed and efficiency. also It helps to save time , the developers can work offline , track changes and undo mistakes .

-> Starting a project:
Create a local repository: $ git init
Make a local copy of the server repository: $ git clone

-> Local changes:
Add a file to staging (Index) area: $ git add <Filename>
Add all files of a repo to staging (Index) area: $ git add .
Record or snapshots the file permanently in the version history with a message: $ git commit -m " Commit Message"

-> Track changes
Track the changes that have not been staged: $ git diff
Display the state of the working directory and the staging area:$ git status

-> Commit History
Display the most recent commits and the status of the head: $ git log

-> gnoring files
Specify intentionally untracked files that Git should ignore. Create .gitignore:
$ touch .gitignore (within add the files to be ignored)

-> Branching: Switch between branches in a repository.
Switch to a particular branch: $ git checkout
Create a new branch and switch to it: $ git checkout -b

-> Merging:
Merge the branches: $ git merge

-> Remote:
Add a remote for the repository: $ git remote add origin (add url)

-> Pushing Updates
Push data to the remote server: $ git push -u origin main
Push data to the remote server (forced): $ git push -f

-> Pulling updates
Pull the data from the server: $ git pull origin master

-> Removing files
Remove the files from the working tree and from the index:$ git rm <file Name>
